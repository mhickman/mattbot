# Description:
#   Example scripts for you to examine and try out.
#
# Notes:
#   They are commented out by default, because most of them are pretty silly and
#   wouldn't be useful and amusing enough for day to day huboting.
#   Uncomment the ones you want to try and experiment with.
#
#   These are from the scripting documentation: https://github.com/github/hubot/blob/master/docs/scripting.md

HttpClient = require('scoped-http-client')

module.exports = (robot) ->

  bloops = ["bloopybloop", "blooooooooop", "bloopasaurausrex"]

  points = {}
  reasons = {}

  catFacts = (res, name) ->
    HttpClient.create('http://catfacts-api.appspot.com/api/facts?number=1').get() (error, response, body) ->
      responseJson = JSON.parse(body)
      res.send "Hey @#{name}! #{responseJson.facts[0]}"

  robot.hear /bloop/i, (res) ->
    res.send bloops[Math.floor(Math.random() * bloops.length)]

  robot.hear /mattbot subscribe (.*) to catfacts/i, (msg) ->
    interval = setInterval(( -> catFacts(msg, msg.match[1])), 15000)

    setTimeout =>
      clearInterval(interval)
    , 120000

  robot.hear /:heart: to (.*) for (.*)/i, (msg) ->
    msg.send "ok... but #{msg.match[1]} probably sucks at #{msg.match[2]} anyways."
    name = msg.match[1]
    if points[name] == undefined
      points[name] = 1
      reasons[name] = [msg.match[2]]
    else
      points[name] = points[name] + 1
      reasons[name].push msg.match[2]

  robot.hear /quest bar/i, (msg) ->
    msg.send "mmmm, quest bars, those things are delicious. hey @rucha did you remember to grab matt some?"

  robot.hear /mattbot score (.*)/i, (msg) ->
    if points[msg.match[1]] == undefined
      msg.send "i don't know anything about #{msg.match[1]} you idiot."
    else
      msg.send "#{msg.match[1]} has #{points[msg.match[1]]} <3's"
      random = reasons[msg.match[1]][Math.floor(Math.random() * reasons[msg.match[1]].length)]
      msg.send "one thing #{msg.match[1]} did was #{random}"

  robot.hear /where do dogs go/i, (res) ->
    res.send "TO THE POUND!!!!111!"
  
  robot.hear /rucha stinks/i, (res) ->
    res.send "I know right, stinky stinky @rucha, ewwwwwwww"
  # robot.hear /badger/i, (res) ->
  #   res.send "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS"
  #
  # robot.respond /open the (.*) doors/i, (res) ->
  #   doorType = res.match[1]
  #   if doorType is "pod bay"
  #     res.reply "I'm afraid I can't let you do that."
  #   else
  #     res.reply "Opening #{doorType} doors"
  #
  robot.hear /I like pie/i, (res) ->
    res.emote "makes a freshly baked pie"
  #
  # lulz = ['lol', 'rofl', 'lmao']
  #
  # robot.respond /lulz/i, (res) ->
  #   res.send res.random lulz
  #
  # robot.topic (res) ->
  #   res.send "#{res.message.text}? That's a Paddlin'"
  #
  #
  # enterReplies = ['Hi', 'Target Acquired', 'Firing', 'Hello friend.', 'Gotcha', 'I see you']
  # leaveReplies = ['Are you still there?', 'Target lost', 'Searching']
  #
  # robot.enter (res) ->
  #   res.send res.random enterReplies
  # robot.leave (res) ->
  #   res.send res.random leaveReplies
  #
  # answer = process.env.HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING
  #
  # robot.respond /what is the answer to the ultimate question of life/, (res) ->
  #   unless answer?
  #     res.send "Missing HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING in environment: please set and try again"
  #     return
  #   res.send "#{answer}, but what is the question?"
  #
  # robot.respond /you are a little slow/, (res) ->
  #   setTimeout () ->
  #     res.send "Who you calling 'slow'?"
  #   , 60 * 1000
  #
  # annoyIntervalId = null
  #
  # robot.respond /annoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #     return
  #
  #   res.send "Hey, want to hear the most annoying sound in the world?"
  #   annoyIntervalId = setInterval () ->
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #   , 1000
  #
  # robot.respond /unannoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "GUYS, GUYS, GUYS!"
  #     clearInterval(annoyIntervalId)
  #     annoyIntervalId = null
  #   else
  #     res.send "Not annoying you right now, am I?"
  #
  #
  # robot.router.post '/hubot/chatsecrets/:room', (req, res) ->
  #   room   = req.params.room
  #   data   = JSON.parse req.body.payload
  #   secret = data.secret
  #
  #   robot.messageRoom room, "I have a secret: #{secret}"
  #
  #   res.send 'OK'
  #
  # robot.error (err, res) ->
  #   robot.logger.error "DOES NOT COMPUTE"
  #
  #   if res?
  #     res.reply "DOES NOT COMPUTE"
  #
  # robot.respond /have a soda/i, (res) ->
  #   # Get number of sodas had (coerced to a number).
  #   sodasHad = robot.brain.get('totalSodas') * 1 or 0
  #
  #   if sodasHad > 4
  #     res.reply "I'm too fizzy.."
  #
  #   else
  #     res.reply 'Sure!'
  #
  #     robot.brain.set 'totalSodas', sodasHad+1
  #
  # robot.respond /sleep it off/i, (res) ->
  #   robot.brain.set 'totalSodas', 0
  #   res.reply 'zzzzz'
